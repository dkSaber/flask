FROM python:latest

# Création du répertoire de l'application
WORKDIR /app

COPY source_code/ /app

# Installation des dépendances Python

RUN pip install -r requirements.txt

EXPOSE 8181

# Commande de démarrage de l'application
CMD [ "python", "server.py" ]
